FROM node:10-alpine

EXPOSE 3000

ADD . /app
WORKDIR /app

RUN npm install

CMD ["npm", "run", "start:local"]
