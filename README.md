# Telegram Channel Publisher

*****

A Telegram publisher function, it can be run standalone (locally or remote) or as a Google Cloud Function (Serverless).

The function receives a JSON object and creates a message to publish in a Telegram channel.

This repository does not include a GUI for creating the JSON object, it is part of an serverless, microservices project architecture for publishing posts in Telegram channels.

The rest of the architecture could be (does not exist yet):
 * Creation UI (webapp, *code named* telegram-channel-capture)
 * Processor (telegram-channel-processor).

## Standalone setup

```bash
# Install all dependencies
yarn

# Run standalone
# Remember that you must have set envVars be it by source .env or manually
yarn start:local

# Use curl for sending a POST request
curl -X POST http://localhost:3000 -H "Content-Type: application/json" -d "{"title": "Curl message!", "content": "A *test* with _Markdown_"}"
```

## Container (Long running) setup

```bash
  # Building the docker image
  docker build -t telegram-publisher .

  # Running an instance of the container
  docker run --rm -d --name publisher -p 3003:3000 -e TELEGRAM_TOKEN={your-bot-token} -e CHANNEL_ID={your-channel-id} telegram-publisher
```

## Serverless setup

This is serverless function that process information from Google Cloud PubSub (other queues will be added later) and publish a message to a Telegram Channel.

