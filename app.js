const Publisher = require('./src/Publisher');
const Errors = require('./src/Errors');
const Server = require('./src/Server');

function getLocals() {
  if (process.env['TELEGRAM_TOKEN'] && process.env['CHANNEL_ID']) {
    return {
      telegramToken: process.env['TELEGRAM_TOKEN'],
      channel: process.env['CHANNEL_ID']
    }
  }

  throw new Errors.TelegramPublisherError(Errors.ERROR_CODES.NoEnvVars);
}

class App {
  constructor(options) {
    const localconf = getLocals();
    const publisher = this.publisher = new Publisher({
      token: localconf.telegramToken,
      channel: localconf.channel
    });
  }

  post(options) {
    return this.publisher.post( options );
  }
}

// It is running as application
if (require.main === module) {
  // Bootstraping the standalone mode
  const server = new Server(new App);
  server.start();
}

module.exports = App;
