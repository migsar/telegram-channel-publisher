// Local config, I will eventually move this to env variables or a key value data store
const localconf =  require('./local.js');

const { Digestor, Publisher, Errors } = require('./src');

/*
 *
 * This is a serverless function triggered by a pubsub topic.
 * Error messages should be logged, there is no HTTP response.
 *
 **/
const channelPublisher = (ev, cb) => {
  const message = ev.data;

  try {
    const data = message.data ? Buffer.from(message.data, 'base64').toString() : null;

    // Try to convert string data to js object
    if (data) {
      const post = JSON.parse(data);
      const callToken = post.token;

      delete post.token;

      if ( callToken === localconf.webhookToken ) {
        const d = new Digestor();
        const p = new Publisher({
          token: localconf.telegramToken,
          channel: localconf.channel
        });

        p.sendMessage( d.process(post) )
          .then(function (ack) {
            console.log('Ack begin');
            console.log(ack.data);
            console.log('Ack end');
            cb();
          })
      } else {
        throw new Errors.TelegramChannelError( Errors.CONSTANTS.Forbidden );
      }
    } else {
      throw new Errors.TelegramChannelError( Errors.CONSTANTS.EmptyPayload );
    }
  } catch (err) {
    console.error(err);
    cb();
  }
};

exports.channelPublisher = channelPublisher;
