const axios = require('axios');

const Digestor = require('./Digestor');
const {
  BOT_API_URL,
  TYPE,
  ENDPOINT
} = require('./Publisher.constants');
const { TelegramPublisherError, ERROR_CODES } = require('./Errors');

/** Class for Telegram Publisher */
class Publisher {
  /**
   * Create a new Telegram Publisher
   * @param {Object} options - Publisher's configuration object.
   */
  constructor(options) {
    this.channel = options.channel;
    this.token = options.token;
    // todo: I think this could be improved... but how?
    this.url = BOT_API_URL.replace('<BOT_TOKEN>', this.token);
  }

  /**
   * Sends a message to the channel
   * @param {string} message - The message to send
   * @param {Object} options - Options
   */
  post(options) {
    const { type } = options;
    const loadParams = this.validateTypeParams(options);

    return axios.post(this.getEndpoint(type), {
      chat_id: this.channel,
      ...loadParams
    });
  }

  /**
   * Get endpoint for the resource
   * @param {string} type - Type of the resource
   */
  getEndpoint(type = TYPE.MESSAGE) {
    const map = {
      [TYPE.MESSAGE]: ENDPOINT.SEND_MESSAGE,
      [TYPE.LOCATION]: ENDPOINT.SEND_LOCATION,
      [TYPE.CONTACT]: ENDPOINT.SEND_CONTACT,
      [TYPE.POLL]: ENDPOINT.SEND_POLL
    };

    return `${this.url}${map[type]}`;
  }

  /**
   * Validate that all required params are provided
   * @param {Object} options - Options
   */
  validateTypeParams(options) {
    const { type } = options;
    const MissingPropertyError = new TelegramPublisherError(ERROR_CODES.NO_TYPE_PROPERTY);
    // chat_id is not included as it is required for all of them
    let d = new Digestor();
    const map = {
      [TYPE.MESSAGE]: {
        ALLOWED: ['title', 'content', 'tags'],
        REQUIRED: ['content', 'tags'],
        process: (options) => {
          return {
            text: d.process(options),
            // parse_mode is either html or markdown.
            // Why not html? Markdown is easier to write
            parse_mode: 'Markdown'
          };
        }
      },
      [TYPE.LOCATION]: {
        ALLOWED: [
          'latitude',
          'longitude',
          'live_period',
          'disable_notification',
          'reply_to_message_id',
          'reply_markup'
        ],
        REQUIRED: ['latitude', 'longitude']
      },
      [TYPE.CONTACT]: {
        ALLOWED: [],
        REQUIRED: []
      },
      [TYPE.POLL]: {
        ALLOWED: ['question', 'options'],
        REQUIRED: ['question', 'options']
      }
    };

    map[type].REQUIRED.forEach(opt => {
      if (!options[opt]) throw MissingPropertyError;
    });

    const cleanOptions = Object.keys(options)
      .filter(opt => map[type].ALLOWED.includes(opt))
      .reduce((acc, opt) => Object.assign(acc, {[opt]: options[opt]}), {});

    if (!map[type].process) return cleanOptions;

    return {
      ...cleanOptions,
      ...map[type].process(options)
    };
  }
}

module.exports = Publisher;
