const ERROR_CODES = {
  Forbidden: 'Forbidden. You don\'t have enough permissions.',
  EmptyPayload: 'The payload is empty or does not follow the expected structure.',
  NoEnvVars: 'Environment variables were not found.',
  NO_GENERAL_PROPERTY: 'Required general property not provided.',
  NO_TYPE_PROPERTY: 'Required type property not provided.'
};

class TelegramPublisherError extends Error {
  constructor(message) {
    super(message);
  }
}

module.exports = {
  TelegramPublisherError,
  ERROR_CODES
}
