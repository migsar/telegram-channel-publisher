module.exports = {
  BOT_API_URL: 'https://api.telegram.org/bot<BOT_TOKEN>/',
  TYPE: {
    MESSAGE: 'message',
    LOCATION: 'location',
    CONTACT: 'contact',
    POLL: 'poll'
  },
  ENDPOINT: {
    SEND_MESSAGE: 'sendMessage',
    SEND_LOCATION: 'sendLocation',
    SEND_CONTACT: 'sendContact',
    SEND_POLL: 'sendPoll'
  }
}
