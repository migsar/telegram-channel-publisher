/** Class for Telegram's message digestor */
class Digestor {
  /**
   * Create a new Telegram Digestor
   * @param {Object} options - Digestor's configuration object.
   */
  constructor(options) {
  }

  /**
   * Creates a Telegram formatted message
   * @param {Object} post - Web capturing UI post object.
   * @param {string} post.title - Post title.
   * @param {string} post.content - Post content.
   */
  process(options = {}) {
    const { title, content, tags } = options;
    return `
      ${tags.map(tag => `#${tag}`).join(' ')} \n *${title || ''}* \n ${content}
    `;
  }
}

module.exports = Digestor;
