const express = require('express');
const bodyParser = require('body-parser');

const { TelegramPublisherError, ERROR_CODES } = require('./Errors');

class Server {
  constructor(publisherApp) {
    const app = this.app = express();
    app.use(bodyParser.json());

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // todo: app should be middleware
    app.post('/', (req, res) => {
      const MissingPropertyError = new TelegramPublisherError(ERROR_CODES.NO_GENERAL_PROPERTY);
      // todo: Allowed must include both our defined params and/or
      //       a valid translation for Telegrams API params
      const allowed = ['title', 'tags', 'type', 'template', 'text'];
      const required = ['type'];
      const options = req.body;
      // Cleaning disabled
      //  .filter(opt => allowed.includes(opt))
      //  .reduce((acc, opt) => Object.assign(acc, {[opt]: extra[opt]}), {});

      required.forEach(opt => {
        if (!options[opt]) throw MissingPropertyError;
      });

      publisherApp.post(options)
        .then(response => {
          const { data } = response;
          res.json({ success: true, data });
        })
        .catch(err => {
          console.error(err);
          res.status(500).json({ success: false, error: err});
        });
    });
  }

  start() {
    let host = process.env['PUBLISHER_HOST'] || '0.0.0.0';
    let port = process.env['PUBLISHER_PORT'] || '3000';
    this.app.listen(port, host, () => {
      console.log(`Listening... ${host}:${port}`);
    });
  }
}

module.exports = Server;
